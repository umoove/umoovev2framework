Pod::Spec.new do |s|

          s.name               = "umooveV2framework"

          s.version            = "2.11.5"

          s.summary         = "The Umoove V2 face & eye tracking framework"

          s.homepage        = "http://www.umoove.me"

          s.author               = "Umoove"

          s.platform            = :ios, "8.0"

          s.source              = { :git => "https://umoove@bitbucket.org/umoove/umoovev2framework.git", :tag => "2.11.5" }

          s.vendored_frameworks = 'Umoove.framework'
          
          s.xcconfig     =  { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/umooveV2framework"' }
    end