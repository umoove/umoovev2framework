/****************************** UMoove iOS Eye API Engine Header **********************************************************************************\
 
 The UmooveEngine is the main interface to the Umoove tracking process, through it a client interacts with the Umoove process.
 
 Disclaimers:
 THE INFORMATION IS FURNISHED FOR INFORMATIONAL USE ONLY, IS SUBJECT TO CHANGE WITHOUT NOTICE,
 AND SHOULD NOT BE CONSTRUED AS A COMMITMENT BY UMOOVE.
 UMOOVE ASSUMES NO RESPONSIBILITY OR LIABILITY FOR ANY ERRORS OR INACCURACIES THAT MAY APPEAR IN THIS DOCUMENT
 OR ANY SOFTWARE THAT MAY BE PROVIDED IN ASSOCIATION WITH THIS DOCUMENT.
 THIS INFORMATION IS PROVIDED "AS IS" AND UMOOVE DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY,
 RELATING TO THE USE OF THIS INFORMATION INCLUDING WARRANTIES RELATING TO FITNESS FOR A PARTICULAR PURPOSE,
 COMPLIANCE WITH A SPECIFICATION OR STANDARD, MERCHANTABILITY OR NONINFRINGEMENT.
 
 Legal Notices:
 
 Copyright © 2017, UMOOVE.
 All Rights Reserved.
 The Umoove logo is a registered trademark of Umoove Company.
 Other brands and names are the property of their respective owners.
 


 \************************************************************************************************************************************************/

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>


// Umoove Engine Protocol
@protocol umooveDelegate <NSObject>

/* core v2 */
- (void) UMJoystickUpdate: (int)x : (int)y;
- (void) UMDirectionsUpdate: (int)x : (int)y;
- (void) UMCursorUpdate: (float)x : (float)y;
- (void) UMGestureUpdate:(int)horizontal : (int)vertical;
- (void) UMAbsolutePosition:(float)x :(float)y;
- (void) UMLinearSpeedUpdate:(float)x : (float)y;
- (void) UMStatesUpdate:(int)state;
/* to enable drastic head movements alerts, gestures must be enabled.
 0 - no alerts
 1 - device not stable
 2 - drastic head movements */
- (void) UMAlertsUpdate:(int)alert;

@end

@interface UMooveEngine : NSObject {
    
    // Umoove Engine Members
    id<umooveDelegate> umooveDelegate;
    int directionsX;
    int directionsY;
    float cursorX, cursorY, absolutePositionX, absolutePositionY;
    int joystickX, joystickY;
    int gestureX, gestureY;
    int state, alert;
    bool stateFlag, _gbraFormat, _externalFrames;
    
}

// Umoove Engine Properties
@property (assign) id<umooveDelegate> umooveDelegate;
@property (readwrite, assign) AVCaptureSession *captureSession;
@property (nonatomic, retain) NSString *deviceType;

// general methods
//- (id) initWithKey:(uint32_t)key;
- (id) initWithKey:(uint32_t)key dynamicLibCode:(uint64_t)code;
- (void) start:(BOOL)autoStart;
- (void) startWithPositions:(CGRect)leftEyeRectPosition rightEyeRectPosition:(CGRect) rightEyeRectPosition variance:(float)variance;
- (void) stop;
- (void) pause;
- (void) terminate;
- (void) reset;
- (void) stopSensors;
- (void) startSensors;

// setters
- (void) setAutoStart:(BOOL)autoStart;
- (void) setStableStart:(BOOL)stableStart;
- (void) setHighResFrame:(BOOL)isHighResFrame;
- (void) setFrontCamera:(BOOL)isFront;
- (void) setStrictEyeMovement:(BOOL)isStrict;

// getters
- (int) getState;
- (int) getAlert;

-(float) getDistance;

// enable functions with their setters

/* core v2 */
- (void) enableJoystick: (BOOL)isOn;

- (void) enableCursor: (BOOL)isOn;
- (void) setCursorSensitivity: (float)sensitivity HorizontalInitialPosition: (float)x VerticalInitialPosition: (float)y;

- (void) enableDirections: (BOOL)isOn;
- (void) setDirectionsSensitivity: (float)sensitivity CenterZone: (float)centerZone Levels: (int)levels;

- (void) enableGestures: (BOOL)isOn;
- (void) setGestureSensitivity: (float)sensitivity PreferenceRatio: (float)ratio AllowConsecutiveGestures: (BOOL)allow;

- (void) enableYesNo: (BOOL)isOn;
- (void) setYesNoSensitivity: (float)sensitivity AmountOfMovements: (int)amount;

- (void) enableLinearSpeed:(BOOL)isOn;

- (void) enableAbsolutePosition:(BOOL)isOn;

- (void) setLossSensitivity:(int)lossSensitivity;





- (NSString*)getVersion;

// funtions used when the client send the frames and are not initiated internally
- (id) initWithExternalFrames:(BOOL)externalFrames withGBRAformat:(BOOL)GBRAformat withKey:(uint32_t)key dynamicLibCode:(uint64_t)code;
- (void) processFrame:(uint8_t*)frame withWidth:(int)width withHeight:(int)height;

@end
